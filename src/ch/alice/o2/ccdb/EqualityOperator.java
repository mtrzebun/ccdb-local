package ch.alice.o2.ccdb;

/**
 * @author costing
 * @since 2023-06-20
 */
public class EqualityOperator extends ConstraintOperator {
	/**
	 * @param value
	 */
	public EqualityOperator(String value) {
		super(value);
	}

	@Override
	public String toSQLExpression() {
		return "=";
	}

	@Override
	public boolean matches(String s) {
		return value.equals(s);
	}

	@Override
	public String getURLExpression() {
		return "=";
	}

	@Override
	public boolean isMeaningful() {
		return value != null && !value.isBlank();
	}

	@Override
	public String toSQLValue() {
		return value;
	}
}
