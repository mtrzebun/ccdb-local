package ch.alice.o2.ccdb.servlets;

import static ch.alice.o2.ccdb.servlets.ServletHelper.printUsage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.user.JAKeyStore;
import ch.alice.o2.ccdb.ConstraintOperator;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.multicast.Blob;
import ch.alice.o2.ccdb.multicast.UDPReceiver;
import ch.alice.o2.ccdb.webserver.EmbeddedTomcat;
import lazyj.Format;

/**
 * @author costing
 * @since 2022-09-29
 */
public class MemoryProxy extends Memory {
	private static final long serialVersionUID = 1L;

	private static final Monitor monitor = MonitorFactory.getMonitor(Memory.class.getCanonicalName());

	static {
		try {
			final SSLSocketFactory sslSocketFactory = JAKeyStore.getSSLSocketFactory();
			HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
		}
		catch (final GeneralSecurityException e2) {
			System.err.println("Could not initialize the SSL context, CERN CA-signed services will not be validated and user-restricted resources will not be accessible:\n" + e2.getMessage());
		}
	}

	@Override
	protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "HEAD_ms")) {
			doGet(request, response, true);
		}
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "GET_ms")) {
			doGet(request, response, false);
		}
	}

	private static void doGet(final HttpServletRequest request, final HttpServletResponse response, final boolean head) throws IOException {
		// task name / detector name [ / time [ / UUID ] | [ / query string]* ]
		// if time is missing - get the last available time
		// query string example: "quality=2"

		final RequestParser parser = new RequestParser(request);

		if (!parser.ok) {
			printUsage(request, response);
			return;
		}

		CCDBUtils.disableCaching(response);

		Blob matchingObject = getMatchingObject(parser);

		String address = UPSTREAM_URL + "/" + parser.path;

		if (parser.startTimeSet)
			address += "/" + parser.startTime;
		if (parser.endTimeSet)
			address += "/" + parser.endTime;
		if (parser.uuidConstraint != null)
			address += "/" + parser.uuidConstraint;
		if (parser.flagConstraints != null && parser.flagConstraints.size() > 0)
			for (final Map.Entry<String, ConstraintOperator> entry : parser.flagConstraints.entrySet())
				address += "/" + Format.encode(entry.getKey()) + entry.getValue().getURLExpression() + Format.encode(entry.getValue().value);

		address += "?HTTPOnly=true";

		// check if this object is the latest
		try {
			final URL uSource = new URL(address);
			final HttpURLConnection conn = (HttpURLConnection) uSource.openConnection();
			conn.setInstanceFollowRedirects(false);

			if (parser.notBefore > 0)
				conn.setRequestProperty("If-Not-Before", String.valueOf(parser.notBefore));

			if (parser.notAfter > 0)
				conn.setRequestProperty("If-Not-After", String.valueOf(parser.notAfter));

			if (matchingObject != null)
				conn.setRequestProperty("If-None-Match", "\"" + matchingObject.getUuid() + "\"");

			conn.addRequestProperty("User-Agent", "MemoryProxy/" + EmbeddedTomcat.getCCDBVersion());

			conn.connect();

			final int code = conn.getResponseCode();

			try (InputStream errStream = conn.getErrorStream()) {
				if (errStream != null) {
					response.sendError(code, conn.getResponseMessage());
					monitor.incrementCounter("errorFromUpstream");
					return;
				}
			}

			try (InputStream sourceStream = conn.getInputStream()) {
				if (code == HttpServletResponse.SC_NOT_MODIFIED && matchingObject != null) {
					// the existing Blob is still valid
					// System.err.println("Reusing the validated content of " + matchingObject.getUuid());
					monitor.incrementCacheHits("memorycache");
				}
				else {
					matchingObject = null;

					if (code >= 200 && code < 400) {
						// populate the headers

						final UUID id = UUID.fromString(Format.replace(conn.getHeaderField("ETag"), "\"", ""));

						final Map<String, List<String>> fields = conn.getHeaderFields();

						if (code >= 200 && code < 300) {
							final ByteArrayOutputStream baos = new ByteArrayOutputStream(conn.getContentLength());
							sourceStream.transferTo(baos);

							matchingObject = new Blob(new LinkedHashMap<String, String>(), baos.toByteArray(), parser.path, id);
							monitor.incrementCacheMisses("memorycache");
						}
						else {
							// let's follow the redirect
							final byte[] content = getContent(conn, 10);

							if (content != null) {
								matchingObject = new Blob(new LinkedHashMap<String, String>(), content, parser.path, id);
								monitor.incrementCacheMisses("memorycache");
							}
						}

						if (matchingObject != null) {
							for (final Map.Entry<String, List<String>> entry : fields.entrySet()) {
								final String key = entry.getKey();

								if (key == null || key.startsWith("Access-Control-") || key.equals("Cache-Control") || key.equals("Content-Location") || key.equals("Date") || key.equals("ETag")
										|| key.equals("Location") || key.equals("Content-Length"))
									continue;

								if (key.equals("Content-Disposition")) {
									final String value = entry.getValue().iterator().next();
									final int idx = value.indexOf("filename=\"");

									if (idx >= 0) {
										final int idx2 = value.indexOf('"', idx + 10);

										matchingObject.setProperty("OriginalFileName", value.substring(idx + 10, idx2));
									}

									continue;
								}

								matchingObject.setProperty(key, entry.getValue().iterator().next());
							}

							matchingObject.startTime = Long.parseLong(matchingObject.getProperty("Valid-From"));
							matchingObject.endTime = Long.parseLong(matchingObject.getProperty("Valid-Until"));

							UDPReceiver.addToCacheContent(matchingObject);
						}
					}
				}
			}
		}
		catch (IOException | NoSuchAlgorithmException ioe) {
			System.err.println("Exception downloading/checking: " + ioe.getMessage());

			ioe.printStackTrace();

			if (matchingObject != null) {
				response.setStatus(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION);
				monitor.incrementCounter("errorChecking");
			}
			else {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error querying the upstream server and no data in memory to fall back to");
				monitor.incrementCounter("errorDownloading");
				return;
			}
		}

		if (matchingObject == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching object found");
			return;
		}

		if (parser.cachedValues.size() > 0 && parser.cachedValues.contains(matchingObject.getUuid())) {
			response.setHeader("ETag", '"' + matchingObject.getUuid().toString() + '"');
			response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
			return;
		}

		setHeaders(matchingObject, response);

		if (!head) {
			download(matchingObject, request, response);
		}
		else {
			response.setContentLengthLong(matchingObject.getPayload().length);
			response.setHeader("Accept-Ranges", "bytes");
			setMD5Header(matchingObject, response);
		}
	}

	private static byte[] getContent(final HttpURLConnection conn, final int limit) {
		if (limit <= 0)
			return null;

		final LinkedHashSet<String> alternativeLocations = new LinkedHashSet<>();

		final Map<String, List<String>> fields = conn.getHeaderFields();

		if (fields.containsKey("Location"))
			alternativeLocations.addAll(fields.get("Location"));
		if (fields.containsKey("Content-Location"))
			alternativeLocations.addAll(fields.get("Content-Location"));

		for (final String alt : alternativeLocations) {
			String altAddr = alt;

			if (altAddr.startsWith("/")) {
				final URL serverURL = conn.getURL();
				altAddr = serverURL.getProtocol() + "://" + serverURL.getHost() + ":" + serverURL.getPort() + altAddr;
			}

			try {
				final URL altURL = new URL(altAddr);
				final HttpURLConnection altConn = (HttpURLConnection) altURL.openConnection();
				altConn.setInstanceFollowRedirects(true);

				try (InputStream is = altConn.getInputStream()) {
					final int status = altConn.getResponseCode();

					if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER || status == 307) {
						final byte[] recursion = getContent(altConn, limit - 1);

						if (recursion != null)
							return recursion;
					}
					else if (status >= 200 && status < 300) {
						final ByteArrayOutputStream baos = new ByteArrayOutputStream();
						is.transferTo(baos);

						return baos.toByteArray();
					}
				}
			}
			catch (final Exception e) {
				System.err.println("Exception contacting " + alt + ": " + e.getMessage());
				e.printStackTrace();
			}
		}

		return null;
	}
}
