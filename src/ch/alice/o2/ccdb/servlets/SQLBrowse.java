package ch.alice.o2.ccdb.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import ch.alice.o2.ccdb.ConstraintOperator;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.servlets.formatters.FormatterFactory;
import ch.alice.o2.ccdb.servlets.formatters.SQLFormatter;
import lazyj.DBFunctions;
import lazyj.Utils;

/**
 * SQL-backed implementation of CCDB. This servlet implements browsing of objects in a particular path
 *
 * @author costing
 * @since 2018-04-26
 */
@WebServlet("/browse/*")
public class SQLBrowse extends ExtraVerbsServlet {
	private static final long serialVersionUID = 1L;

	private static final Monitor monitor = MonitorFactory.getMonitor(SQLBrowse.class.getCanonicalName());

	private static final Comparator<SQLObject> sortBySOV = (o1, o2) -> {
		int diff = o1.getPath().compareTo(o2.getPath());

		if (diff != 0)
			return diff;

		diff = Long.compare(o2.validFrom, o1.validFrom);

		if (diff != 0)
			return diff;

		return o1.compareTo(o2);
	};

	private static final Comparator<SQLObject> sortByEOV = (o1, o2) -> {
		int diff = o1.getPath().compareTo(o2.getPath());

		if (diff != 0)
			return diff;

		diff = Long.compare(o2.validUntil, o1.validUntil);

		if (diff != 0)
			return diff;

		return o1.compareTo(o2);
	};

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response, false);
	}

	@Override
	protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response, true);
	}

	private static void logMessage(final String message) {
		System.err.println(System.currentTimeMillis() + " : SQLBrowse : " + message);
	}

	private static void doGet(final HttpServletRequest request, final HttpServletResponse response, final boolean headRequest) throws IOException {
		try (Timing t = new Timing(monitor, headRequest ? "HEAD_ms" : "GET_ms")) {
			// list of objects matching the request
			// URL parameters are:
			// task name / detector name [ / time [ / UUID ] | [ / query string]* ]
			// if time is missing - get the last available time
			// query string example: "quality=2"

			final RequestParser parser = new RequestParser(request, true);

			/*
			 * if (!parser.ok) {
			 * printUsage(request, response);
			 * return;
			 * }
			 */

			final List<SQLObject> matchingObjects = SQLObject.getAllMatchingObjects(parser);

			final SQLFormatter formatter = FormatterFactory.getFormatter(headRequest ? null : request, parser.latestFlag);

			formatter.setParser(parser);

			response.setContentType(formatter.getContentType());

			CCDBUtils.disableCaching(response);
			CCDBUtils.disableRobots(response);

			final boolean sizeReport = Utils.stringToBool(request.getParameter("report"), false);

			final String prepareArgument = request.getParameter("prepare");

			final boolean syncPrepare = "sync".equalsIgnoreCase(prepareArgument);

			final boolean asyncPrepare = Utils.stringToBool(prepareArgument, false) && !syncPrepare;

			String sortField = request.getParameter("sort");

			if (sortField != null) {
				sortField = sortField.trim().toLowerCase();

				if ("from".equals(sortField) || "sov".equals(sortField))
					Collections.sort(matchingObjects, sortBySOV);
				else if ("until".equals(sortField) || "eov".equals(sortField))
					Collections.sort(matchingObjects, sortByEOV);
			}

			formatter.setExtendedReport(sizeReport);

			if (parser.latestFlag && matchingObjects != null && asyncPrepare) {
				int objectsQueuedForMulticast = 0;
				long sizeQueuedForMulticast = 0;

				logMessage("Async latest prepare requested by " + request.getRemoteAddr() + ", processsing " + matchingObjects.size() + " matching objects");

				for (final SQLObject object : matchingObjects) {
					String message = object.getPath() + "/" + object.validFrom + "/" + object.validUntil + "/" + object.id + " : ";

					if (AsyncMulticastQueue.queueObject(object)) {
						objectsQueuedForMulticast++;
						sizeQueuedForMulticast += object.size;

						message += "sent";
					}
					else
						message += "skipped";

					logMessage(message);
				}

				response.setHeader("ObjectsQueuedForMulticast", String.valueOf(objectsQueuedForMulticast));
				response.setHeader("SizeQueuedForMulticast", String.valueOf(sizeQueuedForMulticast));
				response.setHeader("ObjectsSkippedForMulticast", String.valueOf((matchingObjects.size() - objectsQueuedForMulticast)));
			}

			if (parser.latestFlag && matchingObjects != null && syncPrepare) {
				final Map<String, AtomicLong> headers = new HashMap<>();

				logMessage("Synchronous latest prepare requested by " + request.getRemoteAddr() + ", processsing " + matchingObjects.size() + " matching objects");

				for (final SQLObject object : matchingObjects) {
					String message = object.getPath() + "/" + object.validFrom + "/" + object.validUntil + "/" + object.id + " : ";

					final int ret = AsyncMulticastQueue.syncStageAndMulticast(object);

					switch (ret) {
						case -2:
							headers.computeIfAbsent("GridDownloadErrors", (k) -> new AtomicLong()).incrementAndGet();
							message += "Grid download error !!!";
							break;
						case -1:
							headers.computeIfAbsent("UDPSendingDisabled", (k) -> new AtomicLong()).incrementAndGet();
							message += "Skipped, no UDP sender";
							break;
						case 0:
							headers.computeIfAbsent("RecentObjectsNotSent", (k) -> new AtomicLong()).incrementAndGet();
							message += "Skipped due to having been recently multicasted";
							break;
						case 2:
							headers.computeIfAbsent("ObjectsDownloadedFromGrid", (k) -> new AtomicLong()).incrementAndGet();
							headers.computeIfAbsent("SizeDownloadedFromGrid", (k) -> new AtomicLong()).addAndGet(object.size);
							message += "Downloaded from Grid, ";
							//$FALL-THROUGH$
						case 1:
							headers.computeIfAbsent("ObjectsMulticasted", (k) -> new AtomicLong()).incrementAndGet();
							headers.computeIfAbsent("SizeMulticasted", (k) -> new AtomicLong()).addAndGet(object.size);
							message += "Multicasted";
							break;
						default:
							break;
					}

					logMessage(message);
				}

				for (final Map.Entry<String, AtomicLong> entry : headers.entrySet())
					response.setHeader(entry.getKey(), entry.getValue().toString());
			}

			try (PrintWriter pw = response.getWriter()) {
				formatter.start(pw);

				formatter.header(pw);

				boolean first = true;

				if (matchingObjects != null)
					for (final SQLObject object : matchingObjects) {
						if (first)
							first = false;
						else
							formatter.middle(pw);

						formatter.format(pw, object);
					}

				formatter.footer(pw);

				if (!parser.wildcardMatching) {
					// It is not clear which subfolders to list in case of a wildcard matching of objects. As the full hierarchy was included in the search there is no point in showing them, so just
					// skip
					// this section.
					formatter.subfoldersListingHeader(pw);

					long thisFolderCount = 0;
					long thisFolderSize = 0;

					try (DBFunctions db = SQLObject.getDB()) {
						String prefix = "";

						if (parser.path == null || parser.path.length() == 0)
							db.query("select distinct split_part(path,'/',1) from ccdb_paths order by 1;");
						else {
							int cnt = 0;

							for (final char c : parser.path.toCharArray())
								if (c == '/')
									cnt++;

							db.query("select distinct split_part(path,'/',?) from ccdb_paths where path like ? order by 1;", false, Integer.valueOf(cnt + 2), parser.path + "/%");

							prefix = parser.path + "/";
						}

						final StringBuilder suffix = new StringBuilder();

						if (parser.startTimeSet)
							suffix.append('/').append(parser.startTime);

						if (parser.uuidConstraint != null)
							suffix.append('/').append(parser.uuidConstraint);

						for (final Map.Entry<String, ConstraintOperator> entry : parser.flagConstraints.entrySet())
							suffix.append('/').append(entry.getKey()).append(entry.getValue().getURLExpression()).append(entry.getValue().value);

						first = true;

						while (db.moveNext()) {
							if (first)
								first = false;
							else
								formatter.middle(pw);

							final String folder = prefix + db.gets(1);

							if (sizeReport) {
								try (DBFunctions db2 = SQLObject.getDB()) {
									db2.query("SELECT object_count, object_size FROM ccdb_stats WHERE pathid=(SELECT pathid FROM ccdb_paths WHERE path=?);", false, folder);

									final long ownCount = db2.getl(1);
									final long ownSize = db2.getl(2);

									db2.query("SELECT sum(object_count), sum(object_size) FROM ccdb_stats WHERE pathid IN (SELECT pathid FROM ccdb_paths WHERE path LIKE ?);", false,
											folder + "/%");

									final long subfoldersCount = db2.getl(1);
									final long subfoldersSize = db2.getl(2);

									formatter.subfoldersListing(pw, folder, folder + suffix, ownCount, ownSize, subfoldersCount, subfoldersSize);
								}
							}
							else
								formatter.subfoldersListing(pw, folder, folder + suffix);
						}

						if (sizeReport) {
							db.query("SELECT object_count, object_size FROM ccdb_stats WHERE pathid=(SELECT pathid FROM ccdb_paths WHERE path=?);", false, parser.path);

							thisFolderCount = db.getl(1);
							thisFolderSize = db.getl(2);
						}
					}

					formatter.subfoldersListingFooter(pw, thisFolderCount, thisFolderSize);
				}

				formatter.end(pw);
			}

			t.endTiming();

			RequestLogging.logRequest(parser.latestFlag ? "/latest" : "/browse", headRequest ? "HEAD" : "GET", request, response, parser, t);
		}
	}
}
