package ch.alice.o2.ccdb.servlets;

import static ch.alice.o2.ccdb.servlets.ServletHelper.printUsage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import alien.io.protocols.Xrootd;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.se.SEUtils;
import alien.user.AliEnPrincipal;
import alien.user.UserFactory;
import ch.alice.o2.ccdb.ConstraintOperator;
import ch.alice.o2.ccdb.EqualityOperator;
import ch.alice.o2.ccdb.Options;
import ch.alice.o2.ccdb.RequestParser;
import ch.alice.o2.ccdb.multicast.Utils;
import ch.alice.o2.ccdb.webserver.EmbeddedTomcat;
import ch.alice.o2.ccdb.webserver.RequestFirewall;
import lazyj.DBFunctions;
import lazyj.Format;
import lazyj.cache.ExpirationCache;
import utils.CachedThreadPool;

/**
 * SQL-backed implementation of CCDB. Files reside on this server and/or a separate storage and clients are served directly or redirected to one of the other replicas for the actual file access.
 *
 * @author costing
 * @since 2017-10-13
 */
@WebServlet("/*")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 100)
public class SQLBacked extends ExtraVerbsServlet {
	/**
	 * @author costing
	 * @since Dec 16, 2021
	 */
	public enum GridReplication {
		/**
		 * Grid replication is completely disabled
		 */
		DISABLED,
		/**
		 * Replicate all objects to Grid
		 */
		FULL,
		/**
		 * Only replicate objects for which the `preservation` flag is set to `true`
		 */
		ONDEMAND;

		/**
		 *
		 */
		GridReplication() {
		}

		static GridReplication get(final int level) {
			switch (level) {
				case 1:
					return FULL;
				case 2:
					return ONDEMAND;
				default:
					return DISABLED;
			}
		}
	}

	private static final long serialVersionUID = 1L;

	private static final Monitor monitor = MonitorFactory.getMonitor(SQLBacked.class.getCanonicalName());

	private static Logger logger = Logger.getLogger(SQLBacked.class.getCanonicalName());

	private static List<SQLNotifier> notifiers = new ArrayList<>();

	/**
	 * The base path of the file repository
	 */
	public static final String basePath = Options.getOption("file.repository.location", System.getProperty("user.home") + System.getProperty("file.separator") + "QC");

	/**
	 * Set to `true` on the Online instance, that should not cache any IDs but always use the database values to ensure consistency
	 */
	public static final boolean multiMasterVersion = lazyj.Utils.stringToBool(Options.getOption("multimaster", null), false);

	private static boolean hasGridBacking = false;

	private static boolean hasUDPSender = false;

	private static CachedThreadPool asyncOperations = new CachedThreadPool(16, 1, TimeUnit.SECONDS);

	private static final GridReplication gridReplicationFlag = GridReplication.get(Options.getIntOption("gridreplication.enabled", 0));

	private static final boolean defaultSyncFetch = lazyj.Utils.stringToBool(Options.getOption("syncfetch", null), gridReplicationFlag == GridReplication.ONDEMAND);

	private static final boolean localCopyFirst = lazyj.Utils.stringToBool(Options.getOption("local.copy.first", null), gridReplicationFlag == GridReplication.ONDEMAND);

	private static final boolean allowEndTimeShrink = lazyj.Utils.stringToBool(Options.getOption("allow.end.time.shrink", null), true);

	private static final boolean objectCacheEnabled = lazyj.Utils.stringToBool(Options.getOption("object.cache.enabled", null), false) && !multiMasterVersion;

	private static final ExpirationCache<UUID, Long> objectCache = new ExpirationCache<>(Options.getIntOption("object.cache.size", 100000));

	private static final long objectCacheTimeout = Options.getIntOption("object.cache.timeout", 60) * 1000;

	private static final ConcurrentHashMap<UUID, Long> activeCacheOffers = new ConcurrentHashMap<>();

	static final ScheduledExecutorService recomputeStatisticsScheduler = Executors.newSingleThreadScheduledExecutor(r -> {
		final Thread t = new Thread(r, "SQLStatisticsRefresher");
		t.setDaemon(true);
		return t;
	});

	static final AtomicInteger recomputeStatisticsInnerCounter = new AtomicInteger(0);

	static {
		monitor.addMonitoring("stats", new SQLStatsExporter(null));

		if (objectCacheEnabled)
			monitor.addMonitoring("objectcache_size", (paramNames, paramValues) -> {
				paramNames.add("objectcache_size");
				paramValues.add(Long.valueOf(objectCache.size()));

				paramNames.add("activeCacheQueries");
				paramValues.add(Long.valueOf(activeCacheOffers.size()));
			});

		MonitorFactory.getMonitor("ch.alice.o2.ccdb.servlets.qc_stats").addMonitoring("qc_stats", new SQLStatsExporter("qc"));

		if (gridReplicationFlag == GridReplication.FULL || gridReplicationFlag == GridReplication.ONDEMAND) {
			try {
				SEUtils.getSE(0);

				System.err.println("Grid replication enabled, type " + gridReplicationFlag.name() + ", central services connection tested OK");

				final AliEnPrincipal account = AsyncReplication.getGridUser();

				System.err.println("Grid identity that will be used for copy operations: " + account.getDefaultUser() + ", role " + account.getDefaultRole());

				final String xrdcpPath = Xrootd.getXrdcpPath();
				final String xrdcpVersion = Xrootd.getXrdcpVersion();

				if (xrdcpPath == null)
					System.err.println("xrdcp couldn't be located, will not be able to copy files in/out without it");
				else if (xrdcpVersion == null)
					System.err.println(
							"xrdcp version could not be determined, looks like something is wrong with calling `" + xrdcpPath + " --version`, you should check it out before trying any Grid I/O");
				else
					System.err.println("xrdcp was found on the system, as " + xrdcpPath + ", version " + xrdcpVersion);

				notifiers.add(AsyncReplication.getInstance(gridReplicationFlag));

				hasGridBacking = true;
			}
			catch (@SuppressWarnings("unused") final Throwable t) {
				System.err.println("Grid replication type " + gridReplicationFlag.name() + " is enabled but upstream connection to JCentral doesn't work, disabling replication at this point");
				System.err.println("If this is an expected behavior please set gridreplication.enabled=0");
			}
		}
		else
			System.err.println("Replication to Grid storage elements is disabled, all data will stay on this machine, under " + basePath);

		if (notifiers.size() == 0)
			notifiers.add(SQLLocalRemoval.getInstance());

		final SQLtoUDP udpSender = SQLtoUDP.getInstance();

		if (udpSender != null) {
			notifiers.add(udpSender);
			hasUDPSender = true;
		}

		final SQLtoHTTP httpSender = SQLtoHTTP.getInstance();

		if (httpSender != null)
			notifiers.add(httpSender);

		System.err.println("Validation cache enabled: " + objectCacheEnabled);
	}

	static boolean isLocalCopyFirst() {
		return localCopyFirst;
	}

	/**
	 * @return <code>true</code> if the instance has Grid backing and can upload / download files from SEs
	 */
	public static boolean gridBacking() {
		return hasGridBacking;
	}

	/**
	 * @return <code>true</code> if any unicast or multicast UDP sending is to be done by this instance
	 */
	public static boolean udpSender() {
		return hasUDPSender;
	}

	@Override
	protected void doHead(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "HEAD_ms")) {
			final RequestParser parser = new RequestParser(request);

			try {
				if (!parser.ok) {
					printUsage(request, response);
					return;
				}

				doGet(request, response, parser, true);

				t.endTiming();
			}
			finally {
				RequestLogging.logRequest("/", "HEAD", request, response, parser, t);
			}
		}
	}

	/**
	 * @return the notifiers to take actions on object events
	 */
	static Collection<SQLNotifier> getNotifiers() {
		return Collections.unmodifiableCollection(notifiers);
	}

	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "GET_ms")) {
			final RequestParser parser = new RequestParser(request);

			try {
				if (!parser.ok) {
					printUsage(request, response);
					return;
				}

				doGet(request, response, parser, false);

				t.endTiming();
			}
			finally {
				RequestLogging.logRequest("/", "GET", request, response, parser, t);
			}
		}
	}

	private static void offerToCache(final SQLObject o) {
		if (!objectCacheEnabled || !(o instanceof SQLObjectImpl))
			return;

		final Long now = Long.valueOf(Thread.currentThread().getId());

		final Long active = activeCacheOffers.computeIfAbsent(o.id, (k) -> now);

		if (!active.equals(now))
			return;

		try {
			final SQLObjectImpl s = (SQLObjectImpl) o;

			final Integer pathId = s.getPathId(false);

			if (pathId == null)
				return;

			try (DBFunctions db = SQLObject.getDB()) {
				db.setReadOnly(true);

				db.query(
						"select round(extract(epoch from lower(validity))*1000) as validfrom from ccdb where pathid=? and not isempty(validity * tsrange(to_timestamp(?) AT TIME ZONE 'UTC', to_timestamp(?) AT TIME ZONE 'UTC')) and createtime>? order by 1 asc limit 1;",
						false, pathId, Double.valueOf(s.validFrom / 1000.), Double.valueOf(s.validUntil / 1000.), Long.valueOf(s.createTime));

				if (db.moveNext())
					objectCache.put(o.id, Long.valueOf(db.getl(1)), objectCacheTimeout);
				else
					objectCache.put(o.id, Long.valueOf(s.validUntil), objectCacheTimeout);
			}
		}
		finally {
			activeCacheOffers.remove(o.id);
		}
	}

	private static void doGet(final HttpServletRequest request, final HttpServletResponse response, final RequestParser parser, final boolean head) throws IOException {
		// list of objects matching the request
		// URL parameters are:
		// task name / detector name [ / time [ / UUID ] | [ / query string]* ]
		// if time is missing - get the last available time
		// query string example: "quality=2"

		CCDBUtils.disableCaching(response);

		if (objectCacheEnabled && parser.cachedValues.size() > 0) {
			for (final UUID u : parser.cachedValues) {
				final Long cacheValidUtil = objectCache.get(u);

				if (cacheValidUtil != null && cacheValidUtil.longValue() > parser.startTime) {
					response.setHeader("ETag", "\"" + u + "\"");
					response.sendError(HttpServletResponse.SC_NOT_MODIFIED);

					monitor.incrementCacheHits("objectcache");

					parser.flagConstraints.put("from-object-cache", new EqualityOperator("true"));

					return;
				}
			}

			monitor.incrementCacheMisses("objectcache");
		}

		final SQLObject matchingObject = SQLObject.getMatchingObject(parser);

		if (matchingObject == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
			return;
		}

		final String rPrepare = request.getParameter("prepare");

		final boolean syncPrepare = "sync".equalsIgnoreCase(rPrepare);

		final boolean asyncPrepare = lazyj.Utils.stringToBool(rPrepare, false) || syncPrepare;

		if (syncPrepare || defaultSyncFetch)
			AsyncMulticastQueue.stage(matchingObject);

		if (asyncPrepare)
			AsyncMulticastQueue.queueObject(matchingObject);

		if (parser.cachedValues.size() > 0 && parser.cachedValues.contains(matchingObject.id)) {
			response.setHeader("ETag", "\"" + matchingObject.id + "\"");
			response.sendError(HttpServletResponse.SC_NOT_MODIFIED);

			offerToCache(matchingObject);

			return;
		}

		final String clientIPAddress = request.getRemoteAddr();

		final boolean httpOnly = lazyj.Utils.stringToBool(request.getParameter("HTTPOnly"), false);

		final boolean hasLocalReplica = setAltLocationHeaders(response, matchingObject, clientIPAddress, httpOnly);

		if (hasLocalReplica && localCopyFirst) {
			SQLDownload.download(head, matchingObject, request, response);
			return;
		}

		setHeaders(matchingObject, response);

		final List<String> replicas = matchingObject.getAddresses(clientIPAddress, httpOnly, hasLocalReplica);

		if (replicas.size() > 0)
			CCDBUtils.sendRedirect(response, replicas.iterator().next());
		else
			response.sendError(HttpServletResponse.SC_NOT_FOUND, "No valid replica found for this object");
	}

	private static boolean setAltLocationHeaders(final HttpServletResponse response, final SQLObject obj, final String clientIPAddress, final boolean httpOnly) {
		boolean hasLocalReplica = false;

		for (final Integer replica : obj.replicas)
			if (replica.intValue() == 0 && obj.getLocalFile(false) != null) {
				hasLocalReplica = true;
				break;
			}

		for (final String address : obj.getAddresses(clientIPAddress, httpOnly, hasLocalReplica))
			response.addHeader("Content-Location", address.replace('\n', '|'));

		return hasLocalReplica;
	}

	/**
	 * Write out the MD5 header, if known
	 *
	 * @param obj
	 * @param response
	 */
	static void setMD5Header(final SQLObject obj, final HttpServletResponse response) {
		if (obj.md5 != null && !obj.md5.isEmpty())
			response.setHeader("Content-MD5", obj.md5);
	}

	/**
	 * Set the response headers with the internal and any user-set metadata information
	 *
	 * @param obj
	 * @param response
	 */
	static void setHeaders(final SQLObject obj, final HttpServletResponse response) {
		response.setDateHeader("Date", System.currentTimeMillis());
		response.setHeader("Valid-Until", String.valueOf(obj.validUntil));
		response.setHeader("Valid-From", String.valueOf(obj.validFrom));

		if (obj.initialValidity != obj.validUntil)
			response.setHeader("InitialValidityLimit", String.valueOf(obj.initialValidity));

		response.setHeader("Created", String.valueOf(obj.createTime));
		response.setHeader("ETag", "\"" + obj.id + "\"");

		response.setDateHeader("Last-Modified", obj.getLastModified());

		response.setHeader("Content-Disposition", "filename=\"" + obj.fileName + "\"");
		response.setHeader("Content-Type", obj.getContentType());

		for (final Map.Entry<String, String> metadataEntry : obj.getMetadataKeyValue().entrySet()) {
			response.setHeader(metadataEntry.getKey(), metadataEntry.getValue());
		}
	}

	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		// create the given object and return the unique identifier to it
		// URL parameters are:
		// task name / detector name / start time [/ end time] [ / flag ]*
		// mime-encoded blob is the value to be stored
		// if end time is missing then it will be set to the same value as start time
		// flags are in the form "key=value"

		try (Timing t = new Timing(monitor, "POST_ms")) {
			final RequestParser parser = new RequestParser(request);

			try {
				if (!parser.ok) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Malformed upload request" + (parser.errorMessage != null ? ": " + parser.errorMessage : ""));
					return;
				}

				if (!parser.endTimeSet) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Both start and end of validity interval must be set for a new object");
					return;
				}

				final Collection<Part> parts = request.getParts();

				if (parts.size() == 0) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "POST request doesn't contain the data to upload");
					return;
				}

				if (parts.size() > 1) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "A single object can be uploaded at a time");
					return;
				}

				final AliEnPrincipal account = UserFactory.get(request);

				if (EmbeddedTomcat.getEnforceSSL() > 1) {
					// Role is checked

					if (account == null) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "Show your ID please");
						return;
					}

					if (!canWrite(account, parser.path)) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to write to this path");
						return;
					}
				}

				final Part part = parts.iterator().next();

				final SQLObject newObject = SQLObject.fromRequest(request, parser.path, parser.uuidConstraint);

				final File targetFile = newObject.getLocalFile(true);

				if (targetFile == null) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot create target file to perform the upload");
					return;
				}

				final MessageDigest md5;

				try {
					md5 = MessageDigest.getInstance("MD5");
				}
				catch (@SuppressWarnings("unused") final NoSuchAlgorithmException e) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot initialize the MD5 digester");
					return;
				}

				newObject.size = 0;

				try (FileOutputStream fos = new FileOutputStream(targetFile); InputStream is = part.getInputStream()) {
					final byte[] buffer = new byte[1024 * 16];

					int n;

					while ((n = is.read(buffer)) >= 0) {
						fos.write(buffer, 0, n);
						md5.update(buffer, 0, n);
						newObject.size += n;
					}
				}
				catch (@SuppressWarnings("unused") final IOException ioe) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot upload the blob to the local file " + targetFile.getAbsolutePath());

					if (!targetFile.delete())
						logger.log(Level.WARNING, "Cannot delete target file of failed upload " + targetFile.getAbsolutePath());

					return;
				}

				for (final Map.Entry<String, ConstraintOperator> constraint : parser.flagConstraints.entrySet()) {
					final String key = constraint.getKey();

					final ConstraintOperator op = constraint.getValue();

					if (!RequestParser.isValidMetadataKey(key)) {
						response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Metadata key `" + key + "` has an invalid format");
						return;
					}

					if ("Created".equals(key))
						try {
							newObject.createTime = Long.parseLong(op.value);
						}
						catch (final NumberFormatException nfe) {
							logger.log(Level.WARNING, "The value passed to the `Created` key is not numeric for " + newObject, nfe);
						}
					else
						newObject.setProperty(key, op.value);
				}

				newObject.uploadedFrom = request.getRemoteHost();
				newObject.fileName = part.getSubmittedFileName();
				newObject.setContentType(part.getContentType());
				newObject.md5 = Utils.humanReadableChecksum(md5.digest()); // UUIDTools.getMD5(targetFile);
				newObject.setProperty("partName", part.getName());

				newObject.replicas.add(Integer.valueOf(0));

				newObject.validFrom = parser.startTime;

				newObject.setValidityLimit(parser.endTime);

				// this is just to log the newly created object's ID together with the request details
				parser.flagConstraints.put("ETag", new EqualityOperator(newObject.id.toString()));

				if (!newObject.save(request, account)) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Cannot insert the object in the database");
					return;
				}

				setHeaders(newObject, response);

				final String location = newObject.getAddress(Integer.valueOf(0)).iterator().next();

				response.setHeader("Location", location);
				response.setHeader("Content-Location", location);
				response.sendError(HttpServletResponse.SC_CREATED);

				asyncOperations.execute(() -> {
					for (final SQLNotifier notifier : notifiers) {
						if (notifier instanceof SQLtoUDP) {
							if (lazyj.Utils.stringToBool(newObject.getProperty("forSyncReco"), true))
								AsyncMulticastQueue.queueObject(newObject);
						}
						else
							notifier.newObject(newObject);
					}
				});

				if (monitor != null)
					monitor.addMeasurement("POST_data", newObject.size);
			}
			finally {
				RequestLogging.logRequest("/", "POST", request, response, parser, t);
			}
		}
	}

	private static long lastACLRefresh = 0;

	private static Map<Pattern, Set<String>> acl = null;

	private static synchronized Map<Pattern, Set<String>> getACLs() {
		if (acl == null || System.currentTimeMillis() - lastACLRefresh > 1000 * 60 * 15) {
			final Map<Pattern, Set<String>> temp = new LinkedHashMap<>();

			try (DBFunctions db = SQLObject.getDB()) {
				db.query("SELECT path, allowed FROM ccdb_acl ORDER BY length(path) desc;");

				while (db.moveNext()) {
					try {
						final Pattern p = Pattern.compile("^" + db.gets(1) + "$");
						final StringTokenizer st = new StringTokenizer(db.gets(2), " \r\n\t,;");

						final Set<String> users = new LinkedHashSet<>(st.countTokens());

						while (st.hasMoreTokens())
							users.add(st.nextToken());

						temp.put(p, users);
					}
					catch (final Exception e) {
						logger.log(Level.SEVERE, "Cannot load this acl: `" + db.gets(1) + "` : `" + db.gets(2) + "`", e);
					}
				}
			}

			if (temp.size() == 0)
				temp.put(Pattern.compile(".*"), Set.of("[ccdb]"));

			acl = temp;

			lastACLRefresh = System.currentTimeMillis();
		}

		return acl;
	}

	private static boolean canWrite(final AliEnPrincipal account, final String path) {
		final Set<String> usernames = new HashSet<>(account.getNames());

		for (final String username : usernames) {
			final String match = "Users/" + username.charAt(0) + "/" + username + "/";

			if (path.startsWith(match))
				return true;
		}

		for (final String role : account.getRoles())
			usernames.add("[" + role + "]");

		for (final Map.Entry<Pattern, Set<String>> entry : getACLs().entrySet()) {
			if (entry.getKey().matcher(path).matches()) {
				final Set<String> allowedAccounts = entry.getValue();

				for (final String aa : allowedAccounts)
					if (usernames.contains(aa)) {
						if (aa.charAt(0) != '[')
							account.setDefaultUser(aa);

						return true;
					}
			}
		}

		return false;
	}

	@Override
	protected void doPut(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "PUT_ms")) {
			final RequestParser parser = new RequestParser(request);

			if (!parser.ok) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Malformed change request" + (parser.errorMessage != null ? ": " + parser.errorMessage : ""));
				return;
			}

			try {

				final SQLObject matchingObject = SQLObject.getMatchingObject(parser);

				if (matchingObject == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
					return;
				}

				final AliEnPrincipal account = UserFactory.get(request);

				if (EmbeddedTomcat.getEnforceSSL() > 1) {
					// Role is checked

					if (account == null) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "Show your ID please");
						return;
					}

					if (!canWrite(account, parser.path)) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to modify this path");
						return;
					}
				}

				for (final Map.Entry<String, String[]> param : request.getParameterMap().entrySet())
					if (param.getValue().length > 0) {
						final String key = param.getKey();

						if (!RequestParser.isValidMetadataKey(key)) {
							response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Metadata key `" + key + "` has an invalid format");
							return;
						}

						final String value = param.getValue()[0];

						if (!"preservation".equals(key) || lazyj.Utils.stringToBool(value, false))
							matchingObject.setProperty(key, value);
					}

				if (parser.endTimeSet) {
					if (parser.endTime >= matchingObject.validUntil) {
						// Validity can be extended from both Online and Offline instances, without restrictions
						matchingObject.setValidityLimit(parser.endTime);
					}
					else {
						/*
						 * Validity can be reduced only if
						 * - adjustableEOV metadata is set (to any value) and
						 * - is Online || object was created less than 10 minutes ago
						 */
						if (matchingObject.getMetadataKeyValue().containsKey("adjustableEOV")) {
							if (allowEndTimeShrink || matchingObject.createTime > System.currentTimeMillis() - 1000 * 60 * 10) {
								matchingObject.setProperty("adjustableEOV", null);
								matchingObject.setValidityLimit(parser.endTime);
							}
							else {
								response.sendError(HttpServletResponse.SC_BAD_REQUEST,
										"The EoV of this object cannot be reduced on this instance, it was created " + Format.toInterval(System.currentTimeMillis() - matchingObject.createTime)
												+ " ago");
								return;
							}
						}
						else {
							response.sendError(HttpServletResponse.SC_BAD_REQUEST, "The EoV of this object cannot be reduced since it does not contain an adjustableEOV key");
							return;
						}
					}
				}

				final boolean changed = matchingObject.save(request, account);

				setHeaders(matchingObject, response);

				setAltLocationHeaders(response, matchingObject, null, false);

				if (changed)
					response.sendError(HttpServletResponse.SC_NO_CONTENT);
				else
					response.sendError(HttpServletResponse.SC_NOT_MODIFIED);

				asyncOperations.execute(() -> {
					for (final SQLNotifier notifier : notifiers)
						notifier.updatedObject(matchingObject);
				});
			}
			finally {
				RequestLogging.logRequest("/", "PUT", request, response, parser, t);
			}
		}
	}

	@Override
	protected void doDelete(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "DELETE_ms")) {
			if (Options.getIntOption("delete.disable", 0) != 0) {
				response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Delete operation are not permitted on this instance");
				RequestLogging.logRequest("/", "DELETE", request, response, null, t);
				return;
			}

			if (!RequestFirewall.isDeleteAllowed(request)) {
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "Delete operation not allowed from " + request.getRemoteAddr());
				RequestLogging.logRequest("/", "DELETE", request, response, null, t);
				return;
			}

			final RequestParser parser = new RequestParser(request);

			try {
				if (!parser.ok) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Malformed delete request" + (parser.errorMessage != null ? ": " + parser.errorMessage : ""));
					return;
				}

				final SQLObject matchingObject = SQLObject.getMatchingObject(parser);

				if (matchingObject == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
					return;
				}

				if (!matchingObject.delete()) {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Could not delete the underlying record");
					return;
				}

				setHeaders(matchingObject, response);

				response.sendError(HttpServletResponse.SC_NO_CONTENT);

				asyncOperations.execute(() -> {
					for (final SQLNotifier notifier : notifiers)
						notifier.deletedObject(matchingObject);
				});
			}
			finally {
				RequestLogging.logRequest("/", "DELETE", request, response, parser, t);
			}
		}
	}

	@Override
	protected void doOptions(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "OPTIONS_ms")) {
			response.setHeader("Allow", "GET, POST, PUT, DELETE, OPTIONS, MOVE");

			final RequestParser parser = new RequestParser(request);

			try {
				if (!parser.ok)
					return;

				final SQLObject matchingObject = SQLObject.getMatchingObject(parser);

				if (matchingObject == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
					return;
				}

				setHeaders(matchingObject, response);
			}
			finally {
				RequestLogging.logRequest("/", "OPTIONS", request, response, parser, t);
			}
		}
	}

	static {
		// make sure the database structures exist when the server is initialized
		createDBStructure();

		recomputeStatisticsScheduler.scheduleAtFixedRate(() -> {
			recomputeStatistics();
		}, 0, 5, TimeUnit.MINUTES);
	}

	/**
	 * Create the table structure to hold this object
	 */
	public static void createDBStructure() {
		try (DBFunctions db = SQLObject.getDB()) {
			if (db != null)
				if (db.isPostgreSQL()) {
					System.err.println("SQLBacked version " + EmbeddedTomcat.getCCDBVersion() + " is starting up");

					db.query("CREATE EXTENSION IF NOT EXISTS hstore;", true);

					db.query("CREATE TABLE IF NOT EXISTS ccdb_paths (pathId SERIAL PRIMARY KEY, path text UNIQUE NOT NULL);");
					db.query("CREATE TABLE IF NOT EXISTS ccdb_contenttype (contentTypeId SERIAL PRIMARY KEY, contentType text UNIQUE NOT NULL);");

					db.query(
							"CREATE TABLE IF NOT EXISTS ccdb (id uuid PRIMARY KEY, pathId int NOT NULL REFERENCES ccdb_paths(pathId) ON UPDATE CASCADE, validity tsrange, createTime bigint NOT NULL, replicas integer[], size bigint, "
									+ "md5 uuid, filename text, contenttype int REFERENCES ccdb_contenttype(contentTypeId) ON UPDATE CASCADE, uploadedfrom inet, initialvalidity bigint, metadata hstore, lastmodified bigint);");
					db.query("CREATE INDEX IF NOT EXISTS ccdb_pathId2_idx ON ccdb(pathId);");
					db.query("ALTER TABLE ccdb ALTER validity SET STATISTICS 10000;");
					db.query("CREATE INDEX IF NOT EXISTS ccdb_validity2_idx on ccdb using gist(validity);");

					db.query("CREATE TABLE IF NOT EXISTS ccdb_metadata (metadataId SERIAL PRIMARY KEY, metadataKey text UNIQUE NOT NULL);");
					db.query("CREATE TABLE IF NOT EXISTS config(key TEXT PRIMARY KEY, value TEXT);");

					if (!db.query("SELECT * FROM ccdb LIMIT 0")) {
						System.err.println("Database communication cannot be established, please fix config.properties and/or the PostgreSQL server and try again");
						System.exit(1);
					}

					System.err.println("Database connection is verified to work");

					db.query("CREATE TABLE IF NOT EXISTS ccdb_stats (pathid int primary key, object_count bigint default 0, object_size bigint default 0);");

					db.query("CREATE TABLE IF NOT EXISTS ccdb_helper_table (key text primary key, value int);");

					db.query("INSERT INTO ccdb_helper_table VALUES ('ccdb_stats_tainted', 1) ON CONFLICT (key) DO UPDATE SET value=1;");

					db.query("CREATE OR REPLACE FUNCTION ccdb_increment() RETURNS TRIGGER AS $_$\n" +
							"    BEGIN\n" +
							"        LOCK TABLE ccdb_stats;\n" +
							"        INSERT INTO ccdb_stats (pathid, object_count, object_size) VALUES (NEW.pathid, 1, NEW.size), (0, 1, NEW.size) " +
							"            ON CONFLICT (pathid) DO UPDATE SET object_count=ccdb_stats.object_count+1, object_size=ccdb_stats.object_size+EXCLUDED.object_size;\n" +
							"        RETURN NEW;\n" +
							"    END\n" +
							"$_$ LANGUAGE 'plpgsql';");

					db.query("CREATE OR REPLACE FUNCTION ccdb_decrement() RETURNS TRIGGER AS $_$\n" +
							"    BEGIN\n" +
							"        LOCK TABLE ccdb_stats;\n" +
							"		 UPDATE ccdb_stats SET object_count=object_count-1, object_size=object_size-OLD.size WHERE pathid IN (0, OLD.pathid);\n" +
							"        RETURN NEW;\n" +
							"    END\n"
							+ "$_$ LANGUAGE 'plpgsql';");

					db.query("CREATE OR REPLACE FUNCTION ccdb_updatepath() RETURNS TRIGGER AS $_$\n" +
							"    BEGIN\n" +
							"      IF OLD.pathid != NEW.pathid THEN\n" +
							"        LOCK TABLE ccdb_stats;\n" +
							"		 UPDATE ccdb_stats SET object_count=object_count-1, object_size=object_size-OLD.size WHERE pathid=OLD.pathid;\n" +
							"        UPDATE ccdb_stats SET object_count=object_count+1, object_size=object_size+NEW.size WHERE pathid=NEW.pathid;\n" +
							"      END IF;\n" +
							"      RETURN NEW;\n" +
							"    END\n"
							+ "$_$ LANGUAGE 'plpgsql';");

					db.query("CREATE TRIGGER ccdb_increment_trigger AFTER INSERT ON ccdb FOR EACH ROW EXECUTE PROCEDURE ccdb_increment();", true);
					db.query("CREATE TRIGGER ccdb_decrement_trigger AFTER DELETE ON ccdb FOR EACH ROW EXECUTE PROCEDURE ccdb_decrement();", true);
					db.query("CREATE TRIGGER ccdb_update_trigger AFTER UPDATE ON ccdb FOR EACH ROW EXECUTE PROCEDURE ccdb_updatepath();", true);

					db.query("CREATE TABLE IF NOT EXISTS ccdb_acl (path text primary key, allowed text);");

					// cache-less utils

					final HashMap<String, String> idNameInTable = new HashMap<>();
					idNameInTable.put("ccdb_paths", "pathid");
					idNameInTable.put("ccdb_contenttype", "contentTypeId");

					final HashMap<String, String> valueNameInTable = new HashMap<>();
					valueNameInTable.put("ccdb_paths", "path");
					valueNameInTable.put("ccdb_contenttype", "contentType");

					final String[] tables = { "ccdb_paths", "ccdb_contenttype" };
					for (final String tableName : tables) {
						final String idName = idNameInTable.get(tableName);
						final String valueName = valueNameInTable.get(tableName);
						db.query("create or replace function " + tableName + "_latest (value_to_insert text) returns bigint as $$\n"
								+ "begin\n"
								+ "    return (select " + idName + " from " + tableName + " where " + valueName + " = value_to_insert);\n"
								+ "end $$ language plpgsql;");
					}

					db.query("create or replace function ccdb_metadata_latest_keyid_value (values_to_insert hstore) returns hstore as $$\n" +
							"declare \n" +
							"	actual_ids_to_insert hstore := ''::hstore;\n" +
							"	value text;\n" +
							"begin\n" +
							"	foreach value in array akeys(values_to_insert) loop\n" +
							"		actual_ids_to_insert := actual_ids_to_insert || hstore((select metadataId from ccdb_metadata where metadataKey = value)::text, values_to_insert -> value);\n" +
							"	end loop;\n" +
							"	return actual_ids_to_insert;\n" +
							"end\n" +
							"$$ language plpgsql;");
					db.query("create or replace function ccdb_metadata_latest_key_value (keyid_value hstore) returns hstore as $$\n" +
							"declare \n" +
							"    key_value hstore := ''::hstore;\n" +
							"    id integer;\n" +
							"begin\n" +
							"    foreach id in array akeys(keyid_value) loop\n" +
							"        key_value := key_value || hstore((select metadataKey from ccdb_metadata where metadataId = id)::text, keyid_value -> cast(id as text));\n" +
							"    end loop;\n" +
							"    return key_value;\n" +
							"end\n" +
							"$$ language plpgsql;\n");
					db.query("create or replace view ccdb_view as \n" +
							"    select ccdb.*, \n" +
							"        paths.path as path, \n" +
							"        ctype.contenttype as contenttype_value, \n" +
							"        ccdb_metadata_latest_key_value(ccdb.metadata) as metadata_key_value\n" +
							"    from ccdb  \n" +
							"        left outer join ccdb_paths as paths on ccdb.pathid = paths.pathid\n" +
							"        left outer join ccdb_contenttype as ctype on ccdb.contenttype = ctype.contenttypeid;");
				}
				else
					throw new IllegalArgumentException("Only PostgreSQL support is implemented at the moment");
		}
	}

	private static void recomputeStatistics() {
		try (DBFunctions db = SQLObject.getDB(); DBFunctions db2 = SQLObject.getDB()) {
			db.query("SELECT value FROM ccdb_helper_table WHERE key = 'ccdb_stats_tainted';");

			recomputeStatisticsInnerCounter.incrementAndGet();

			if (db.geti(1) == 1 || recomputeStatisticsInnerCounter.get() > 12) {
				try (Timing t = new Timing(monitor, "statistics_recompute")) {
					// System.err.println("Updating statistics, tainted=" + db.geti(1) + ", counter=" + recomputeStatisticsInnerCounter.get());
					db.query("select distinct pathid from (select pathid from ccdb_paths union select pathid from ccdb_stats) x;");

					// The update method below is not optimal, but avoids locking `ccdb` for the duration of a potentially large operation
					while (db.moveNext()) {
						final Integer pathId = Integer.valueOf(db.geti(1));

						db2.query("SELECT count(1), sum(size) FROM ccdb WHERE pathid=?", false, pathId);

						if (db2.moveNext() && db2.geti(1) > 0) {
							db2.query("BEGIN;" +
									"LOCK TABLE ccdb_stats;" +
									"INSERT INTO ccdb_stats (pathid, object_count, object_size) VALUES (?, ?, ?) " +
									"    ON CONFLICT (pathid) DO UPDATE SET object_count=excluded.object_count, object_size=excluded.object_size;" +
									"COMMIT;", false, pathId, Integer.valueOf(db2.geti(1)), Long.valueOf(db2.getl(2)));
						}
						else {
							db2.query("BEGIN;" +
									"LOCK TABLE ccdb_stats;" +
									"DELETE FROM ccdb_stats WHERE pathid=?; " +
									"COMMIT;", false, pathId);
						}
					}

					db.query("BEGIN;" +
							"LOCK TABLE ccdb_stats;" +
							"DELETE FROM ccdb_stats WHERE object_count<=0;" +
							"COMMIT;");

					db.query("UPDATE ccdb_helper_table SET value = 0 WHERE key = 'ccdb_stats_tainted';");

					db.query("SELECT pathId FROM ccdb_paths LEFT OUTER JOIN ccdb_stats USING(pathId) WHERE coalesce(object_count,0)=0;");

					while (db.moveNext()) {
						final Integer pathID = Integer.valueOf(db.geti(1));

						if (db2.query("DELETE FROM ccdb_paths WHERE pathid=?;", false, pathID))
							SQLObject.removePathID(pathID);
					}

					recomputeStatisticsInnerCounter.set(0);
				}
			}
		}
	}

	@Override
	protected void doMove(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		try (Timing t = new Timing(monitor, "MOVE_ms")) {
			final RequestParser parser = new RequestParser(request);

			try {
				if (!parser.ok) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Malformed move request" + (parser.errorMessage != null ? ": " + parser.errorMessage : ""));
					return;
				}

				final SQLObject matchingObject = SQLObject.getMatchingObject(parser);

				if (matchingObject == null) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, "No matching objects found");
					return;
				}

				final String targetPath = request.getHeader("Destination");

				if (targetPath == null || targetPath.isBlank()) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "This method only supports changing the path and you haven't supplied a `Destination` header");
					return;
				}

				if (!targetPath.matches("^([a-zA-Z0-9_.~-]+)(/[a-zA-Z0-9_.~-]+)*$")) {
					response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "Illegal target path specification: " + targetPath);
					return;
				}

				if (targetPath.equals(parser.path)) {
					response.sendError(HttpServletResponse.SC_NOT_MODIFIED, "Source and target are the same");
					return;
				}

				final AliEnPrincipal account = UserFactory.get(request);

				if (EmbeddedTomcat.getEnforceSSL() > 1) {
					// Role is checked

					if (account == null) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "Show your ID please");
						return;
					}

					if (!canWrite(account, parser.path)) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to modify the source path of this object");
						return;
					}

					if (!canWrite(account, targetPath)) {
						response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to modify the target path");
						return;
					}
				}

				if (matchingObject.moveTo(targetPath, request, account))
					response.sendError(HttpServletResponse.SC_NO_CONTENT, "The object path was modified");
				else
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "The object could not be modified");
			}
			finally {
				RequestLogging.logRequest("/", "MOVE", request, response, parser, t);
			}
		}
	}
}
