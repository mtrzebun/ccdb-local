package ch.alice.o2.ccdb.servlets;

import java.util.UUID;

import alien.catalogue.GUIDUtils;
import ch.alice.o2.ccdb.Options;
import ch.alice.o2.ccdb.servlets.SQLBacked.GridReplication;
import lazyj.DBFunctions;

/**
 * Iterate through objects missing a Grid copy and upload them
 *
 * @author costing
 * @since 2021-10-28
 */
public class RetryCopyToGrid {
	/**
	 * Entry point
	 *
	 * @param args
	 */
	public static void main(final String[] args) {
		if (args == null || args.length == 0) {
			System.err.println("Pass as arguments the objects that you want replicated, or '*' for all objects (that miss the Grid replica)");
			return;
		}

		final GridReplication replicationType = GridReplication.get(Options.getIntOption("gridreplication.enabled", 0));

		final AsyncReplication replication = AsyncReplication.getInstance(replicationType);

		if (replication == null) {
			System.err.println("Grid replication is not enabled, please indicate a `-Dgridreplication.enabled={1,2}` (1==ALL, 2==ONDEMAND)");
			return;
		}

		System.err.println("Grid replication instance: " + replication);

		String q = SQLObjectImpl.selectAllFromCCDB + " where -1 != all(replicas)";

		if (replicationType == GridReplication.ONDEMAND) {
			final Integer mdID = SQLObject.getMetadataID("preservation", false);

			if (mdID == null) {
				System.err.println("The `preservation` metadata is not defined yet, no work for me");
				return;
			}

			q += " AND metadata -> '" + mdID + "' IN ('move-to-grid-failed', 'true')";
		}

		for (final String arg : args) {
			try (DBFunctions db = SQLObject.getDB()) {
				if (GUIDUtils.isValidGUID(arg)) {
					db.query(q + " AND id=?", false, UUID.fromString(arg));
				}
				else if ("*".equals(arg)) {
					db.query(q);
				}
				else {
					System.err.println("Don't know how to process argument `" + arg + "`");
					continue;
				}

				while (db.moveNext()) {
					final SQLObject obj = SQLObject.fromDb(db);

					if (replicationType == GridReplication.ONDEMAND) {
						obj.setProperty("preservation", "true");
						obj.tainted = true;
						obj.save(null, null);
					}

					System.err.println("Notifying with: " + obj);

					replication.newObject(obj);
				}
			}
		}

		replication.shutDown();
	}
}
