package ch.alice.o2.ccdb.servlets;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.user.UserFactory;
import ch.alice.o2.ccdb.Options;
import ch.alice.o2.ccdb.RequestParser;
import lazyj.Format;

/**
 * Utility class for logging HTTP requests
 * 
 * @author costing
 * @since 2022-05-13
 */
public class RequestLogging {

	static final String accessLogFile = Options.getOption("access_log", null);

	static final String jsAccessLogFile = Options.getOption("js_access_log", null);

	static final int minExitCodeLogging = Options.getIntOption("access_log_min_level", 400);

	private static final Monitor monitor = MonitorFactory.getMonitor(RequestLogging.class.getCanonicalName());

	static void logRequest(final String servlet, final String method, final HttpServletRequest request, final HttpServletResponse response, final RequestParser parser, final Timing timing) {
		if (accessLogFile == null && jsAccessLogFile == null)
			return;

		final int status = response.getStatus();

		if (status < minExitCodeLogging)
			return;

		final X509Certificate cert[] = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

		String dn = null;

		if (cert != null && cert.length > 0)
			dn = UserFactory.transformDN(cert[0].getSubjectX500Principal().getName());

		if (dn != null)
			monitor.incrementCacheHits("ssl");
		else
			monitor.incrementCacheMisses("ssl");

		final String userAgent = request.getHeader("User-Agent");

		final long timestamp = System.currentTimeMillis();

		final String forwardedFor = request.getHeader("X-Forwarded-For");

		if (accessLogFile != null) {
			final StringBuilder log = new StringBuilder();

			log.append(timestamp).append(" \"");
			log.append(new Date(timestamp)).append("\" ");

			String addr = request.getRemoteAddr();
			if (addr.contains(":"))
				addr = "[" + addr + "]";
			addr += ":" + request.getRemotePort();

			if (forwardedFor != null)
				addr += "," + forwardedFor;

			log.append(addr).append(' ');
			log.append(status).append(' ');
			log.append(method).append(" ");
			log.append(servlet).append(" \"");

			if (parser != null)
				log.append(parser.toCompactString());
			else
				log.append(request.getPathInfo());

			log.append("\" \"");
			log.append(userAgent).append("\" ");

			if (timing != null)
				log.append(timing.getMillis());

			if (dn != null)
				log.append(" \"").append(dn).append('"');

			logLine(log.toString());
		}

		if (jsAccessLogFile != null) {
			final Map<String, Object> js = new LinkedHashMap<>();

			js.put("timestamp", Long.valueOf(timestamp));
			js.put("address", request.getRemoteAddr());
			js.put("port", Integer.valueOf(request.getRemotePort()));
			js.put("servlet", servlet);
			js.put("method", method);
			js.put("status", Integer.valueOf(status));

			if (timing != null)
				js.put("elapsed_ms", Double.valueOf(timing.getMillis()));

			if (parser != null)
				parser.fillDetails(js);
			else
				js.put("path", request.getPathInfo());

			if (userAgent != null)
				js.put("userAgent", userAgent);

			if (dn != null)
				js.put("dn", dn);

			if (forwardedFor != null)
				js.put("forwardedFor", forwardedFor);

			logJSLine(Format.toJSON(js, false).toString());
		}
	}

	private static PrintWriter logPrintWriter = null;
	private static int writtenLogLines = 0;

	private static synchronized void logLine(final String line) {
		try {
			if (logPrintWriter == null) {
				logPrintWriter = new PrintWriter(new FileWriter(accessLogFile, true));
				writtenLogLines = 0;
			}

			logPrintWriter.println(line);
			writtenLogLines++;

			if (writtenLogLines > 1000) {
				logPrintWriter.close();
				logPrintWriter = null;
			}
			else
				logPrintWriter.flush();
		}
		catch (@SuppressWarnings("unused") final Throwable t) {
			logPrintWriter = null;
		}
	}

	private static PrintWriter logJSPrintWriter = null;
	private static int jswrittenLogLines = 0;

	private static synchronized void logJSLine(final String line) {
		try {
			if (logJSPrintWriter == null) {
				logJSPrintWriter = new PrintWriter(new FileWriter(jsAccessLogFile, true));
				jswrittenLogLines = 0;
			}

			logJSPrintWriter.println(line);
			jswrittenLogLines++;

			if (jswrittenLogLines > 1000) {
				logJSPrintWriter.close();
				logJSPrintWriter = null;
			}
			else
				logJSPrintWriter.flush();
		}
		catch (@SuppressWarnings("unused") final Throwable t) {
			logJSPrintWriter = null;
		}
	}

}
