package ch.alice.o2.ccdb.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.catalogue.GUID;
import alien.catalogue.GUIDUtils;
import alien.catalogue.LFN;
import alien.catalogue.PFN;
import alien.catalogue.access.AccessType;
import alien.catalogue.access.AuthorizationFactory;
import alien.config.ConfigUtils;
import alien.io.IOUtils;
import alien.io.protocols.Factory;
import alien.io.protocols.Xrootd;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.se.SE;
import alien.se.SEUtils;
import alien.shell.commands.JAliEnCOMMander;
import alien.test.cassandra.tomcat.Options;
import alien.user.AliEnPrincipal;
import alien.user.UserFactory;
import ch.alice.o2.ccdb.servlets.SQLBacked.GridReplication;
import lazyj.DBFunctions;
import lazyj.StringFactory;
import lazyj.Utils;

/**
 * Physical removal of files is expensive so don't make the client wait until it happens but instead return control immediately and do the physical removal asynchronously
 *
 * @author costing
 * @since 2018-06-08
 */
public class AsyncReplication extends Thread implements SQLNotifier {
	private static final Monitor monitor = MonitorFactory.getMonitor(AsyncReplication.class.getCanonicalName());

	private static Logger logger = Logger.getLogger(AsyncReplication.class.getCanonicalName());

	private static JAliEnCOMMander commander = null;

	private final GridReplication flag;

	private static final Integer LOCAL_REPLICA = Integer.valueOf(0);

	private static final Integer ALIEN_REPLICA = Integer.valueOf(-1);

	/**
	 * @param flag <code>1</code> for replicating all objects to Grid, <code>2</code> to only replicate the ones flagged with <code>"preservation=true"</code>
	 */
	private AsyncReplication(final GridReplication flag) {
		this.flag = flag;
	}

	private static class AsyncReplicationTarget implements Runnable {
		final SQLObject object;
		final SE se;

		public AsyncReplicationTarget(final SQLObject object, final SE se) {
			this.object = object;
			this.se = se;
		}

		@Override
		public void run() {
			final File localFile = object.getLocalFile(false);

			if (localFile == null || !localFile.exists()) {
				System.err.println("No local file to read from");
				return;
			}

			final Integer seNumber = Integer.valueOf(se.seNumber);

			if (object.replicas.contains(seNumber))
				return;

			final GUID guid = object.toGUID();

			if (guid == null)
				return;

			for (final String address : object.getAddress(seNumber)) {
				final PFN newpfn = new PFN(address, guid, se);

				final String reason = AuthorizationFactory.fillAccess(newpfn, AccessType.WRITE);

				if (reason != null) {
					System.err.println("Cannot get the write envelope for " + newpfn.getPFN() + ", reason is: " + reason);
					return;
				}

				final Xrootd xrootd = Factory.xrootd;

				try {
					xrootd.put(newpfn, localFile);

					try (DBFunctions db = SQLObject.getDB()) {
						db.query("update ccdb set replicas=replicas || ? where id=? and NOT ? = ANY(replicas);", false, seNumber, object.id, seNumber);

						object.replicas.add(seNumber);

						break;
					}
				}
				catch (final IOException e) {
					System.err.println("Could not upload to: " + newpfn.pfn + ", reason was: " + e.getMessage());
				}
			}
		}
	}

	private static class AliEnReplicationTarget implements Runnable {
		final SQLObject object;
		private GridReplication flag;
		private int retries = 10;

		public AliEnReplicationTarget(final SQLObject object, final GridReplication flag) {
			this.object = object;
			this.flag = flag;
		}

		@Override
		public void run() {
			final File localFile = object.getLocalFile(false);

			if (localFile == null || !localFile.exists()) {
				System.err.println("No local file to read from for: " + SQLBacked.basePath + "/" + object.getFolder() + "/" + object.id);
				return;
			}

			if (object.replicas.contains(ALIEN_REPLICA))
				return;

			String targetObjectPath = object.getAddress(ALIEN_REPLICA, null, false).iterator().next();

			if (targetObjectPath.startsWith("alien://"))
				targetObjectPath = targetObjectPath.substring(8);

			if (commander == null)
				commander = new JAliEnCOMMander(getGridUser(), null, "CERN", null);

			final LFN l;

			try {
				l = commander.c_api.getLFN(targetObjectPath);
			}
			catch (final Exception e) {
				logger.log(Level.WARNING, "Exception querying the catalogue for " + targetObjectPath + ", retries left " + retries, e);

				if (--retries > 0)
					queueObject(object, this);

				return;
			}

			if (l != null) {
				if (l.size == object.size && object.md5.equals(l.getMD5())) {
					System.err.println("Target LFN existed and details match the current object, updating DB with the pointer to:\n" + l);

					object.replicas.add(ALIEN_REPLICA);
					object.updateReplicas();
				}
				else {
					System.err.println("Target LFN exists but details don't match:\n" + l);
				}

				return;
			}

			final String targetStorageTag = object.getPath().startsWith("TPC/Calib/IDC_DELTA_") ? "tpcidc" : "ccdb";

			try (Timing t = new Timing(monitor, "grid_upload_ms")) {
				final String[] args = new String[flag == GridReplication.ONDEMAND ? 3 : 2];
				args[0] = "-S";
				args[1] = targetStorageTag + ":1";

				// QCDB case - move the files to Grid
				if (flag == GridReplication.ONDEMAND)
					args[2] = "-d";

				final LFN result = IOUtils.upload(localFile, targetObjectPath, commander, args);

				if (result != null) {
					object.replicas.add(ALIEN_REPLICA);

					if (flag == GridReplication.ONDEMAND)
						object.replicas.remove(LOCAL_REPLICA);

					object.updateReplicas();

					final Integer targetReplicas = Integer.valueOf(ConfigUtils.getConfig().geti("AliEnCCDBReplicas", 8));

					final Map<String, Long> ret = commander.c_api.mirrorLFN(targetObjectPath, null, null, Map.of(targetStorageTag, targetReplicas), false, Integer.valueOf(10), null);

					if (ret == null || ret.size() == 0)
						System.err.println("The mirror command didn't return any scheduled transfer for " + targetObjectPath);
					else if (ret.size() < targetReplicas.intValue())
						logger.log(Level.FINE, "Will create only " + ret.size() + " additional replicas out of " + targetReplicas + " requested for " + targetObjectPath + " @ " + targetStorageTag);
				}
				else {
					System.err.println("Failed to upload " + localFile + " to " + targetObjectPath + " with the following error message from AliEn:\n[" + commander.getLastExitCode() + "] "
							+ commander.getLastErrorMessage());

					if (flag == GridReplication.ONDEMAND) {
						// This can come long after the object was initially updated and changes might have been done to it. Refresh the object from the database.
						final SQLObject o2 = SQLObject.getObject(object.id);

						if (o2 != null) {
							o2.setProperty("preservation", "move-to-grid-failed");
							o2.tainted = true;
							o2.save(null, null);
						}
						else
							System.err.println("Cannot flag object ID " + object.id + " as failed to replicate to Grid, the object is no longer present in the database");
					}
				}
			}
			catch (final IOException e) {
				System.err.println("Could not upload " + localFile.getAbsolutePath() + " to " + targetObjectPath + ", reason was: " + e.getMessage());
			}
		}
	}

	private static AliEnPrincipal gridUser = null;

	/**
	 * @return
	 */
	static synchronized AliEnPrincipal getGridUser() {
		if (gridUser != null)
			return gridUser;

		gridUser = AuthorizationFactory.getDefaultUser();

		if (gridUser == null) {
			System.err.println("No Grid identity could be loaded");
			return null;
		}

		final String targetRole = Options.getOption("grid.username", "alidaq");

		if (gridUser.canBecome(targetRole)) {
			System.err.println("Grid account '" + gridUser.getDefaultUser() + "' can become '" + targetRole + "', using this role to upload files");
			gridUser = UserFactory.getByUsername("alidaq");
		}
		else
			System.err.println("Grid account '" + gridUser.getDefaultUser() + "' cannnot become '" + targetRole + "'. Keeping current identity and hope it can upload ...");

		return gridUser;
	}

	@Override
	public void run() {
		while (true) {
			try {
				final Runnable target = asyncReplicationQueue.take();

				if (target instanceof ShutdownTask) {
					System.err.println("Grid replication shutting down");
					return;
				}

				target.run();
			}
			catch (final InterruptedException e) {
				e.printStackTrace();
				return;
			}
			catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	private final BlockingQueue<Runnable> asyncReplicationQueue = new LinkedBlockingQueue<>();

	private static AsyncReplication instance = null;

	/**
	 * @return singleton
	 */
	static synchronized AsyncReplication getInstance(final GridReplication flag) {
		if (flag == null)
			return instance;

		if (instance == null && flag.ordinal() >= 1) {
			instance = new AsyncReplication(flag);
			instance.start();

			monitor.addMonitoring("grid_repl_queue", (names, values) -> {
				names.add("queue_length");
				values.add(Double.valueOf(instance.asyncReplicationQueue.size()));
			});
		}

		return instance;
	}

	private static volatile long lastRefreshed = 0;

	private static volatile List<String> targetSEs = null;

	private static List<String> getTargetSEs() {
		if (System.currentTimeMillis() - lastRefreshed > 1000 * 60) {
			lastRefreshed = System.currentTimeMillis();

			List<String> newValue = null;

			try (DBFunctions db = SQLObject.getDB()) {
				db.query("SELECT value FROM config WHERE key='replication.ses';");

				if (db.moveNext()) {
					final StringTokenizer st = new StringTokenizer(db.gets(1), ",; \t\n\r");

					if (st.hasMoreTokens()) {
						newValue = new ArrayList<>(st.countTokens());

						while (st.hasMoreTokens())
							newValue.add(st.nextToken());
					}
				}
			}

			targetSEs = newValue != null && newValue.size() > 0 ? newValue : Arrays.asList("ALIEN");
		}

		return targetSEs;
	}

	private static boolean queueObject(final SQLObject object, final Runnable task) {
		final AsyncReplication repl = getInstance(GridReplication.DISABLED);

		if (repl != null && (repl.flag == GridReplication.FULL || Utils.stringToBool(object.getProperty("preservation"), false)))
			return repl.asyncReplicationQueue.offer(task);

		return false;
	}

	@Override
	public void newObject(final SQLObject object) {
		final List<String> targets = getTargetSEs();

		if (targets == null || targets.isEmpty())
			return; // nothing to do, but it's expected to be ok

		for (final String seName : targets) {
			if (seName.contains("::"))
				queueMirror(object, seName);
			else if ("ALIEN".equalsIgnoreCase(seName))
				queueObject(object, new AliEnReplicationTarget(object, flag));
			else
				System.err.println("Don't know how to handle the replication target of " + seName);
		}
	}

	/**
	 * @param object
	 * @param seName
	 * @return <code>true</code> if the operation was successfully queued
	 */
	static boolean queueMirror(final SQLObject object, final String seName) {
		try {
			final SE se = SEUtils.getSE(seName);

			if (se != null)
				return queueMirror(object, se);
		}
		catch (final Throwable t) {
			System.err.println("Could not call getSE(" + seName + ") : " + t.getMessage());
		}

		return false;
	}

	/**
	 * @param object
	 * @param se
	 * @return <code>true</code> if the operation was successfully queued
	 */
	static boolean queueMirror(final SQLObject object, final SE se) {
		return queueObject(object, new AsyncReplicationTarget(object, se));
	}

	private static boolean deleteGridReplicas(final SQLObject object) {
		final Iterator<Integer> replicaIterator = object.replicas.iterator();

		boolean anyChange = false;

		while (replicaIterator.hasNext()) {
			final Integer replica = replicaIterator.next();

			if (replica.intValue() == 0) {
				// ignore the local files
				continue;
			}

			if (replica.intValue() < 0) {
				String url = object.getAddress(replica, null, false).iterator().next();

				if (url.startsWith("alien://"))
					url = url.substring(8);

				JAliEnCOMMander.getInstance().c_api.removeLFN(url);
			}
			else {
				final SE se = SEUtils.getSE(replica.intValue());

				if (se != null) {
					final GUID guid = GUIDUtils.getGUID(object.id, true);

					if (guid.exists())
						// It should not exist in AliEn, these UUIDs are created only in CCDB's space
						continue;

					guid.size = object.size;
					guid.md5 = StringFactory.get(object.md5);
					guid.owner = StringFactory.get("ccdb");
					guid.gowner = StringFactory.get("ccdb");
					guid.perm = "755";
					guid.ctime = new Date(object.createTime);
					guid.expiretime = null;
					guid.type = 0;
					guid.aclId = -1;

					for (final String address : object.getAddress(replica)) {
						final PFN delpfn = new PFN(address, guid, se);

						final String reason = AuthorizationFactory.fillAccess(delpfn, AccessType.DELETE);

						if (reason != null) {
							System.err.println("Cannot get the access tokens to remove this pfn: " + delpfn.getPFN() + ", reason is: " + reason);
							continue;
						}

						final Xrootd xrd = Factory.xrootd;

						try {
							if (!xrd.delete(delpfn))
								System.err.println("Cannot physically remove this file: " + delpfn.getPFN());
						}
						catch (final IOException e) {
							System.err.println("Exception removing this pfn: " + delpfn.getPFN() + " : " + e.getMessage());
						}
					}
				}
			}

			replicaIterator.remove();
			anyChange = true;
		}

		return anyChange;
	}

	private static void deleteLocalReplica(final SQLObject object) {
		if (object.replicas.contains(LOCAL_REPLICA)) {
			final File f = object.getLocalFile(false);

			if (f != null && !f.delete())
				logger.log(Level.WARNING, "Cannot remove local file " + f.getAbsolutePath());

			object.replicas.remove(LOCAL_REPLICA);
		}
	}

	@Override
	public void deletedObject(final SQLObject object) {
		deleteLocalReplica(object);

		deleteGridReplicas(object);
	}

	@Override
	public void updatedObject(final SQLObject object) {
		if (flag.ordinal() > 1) {
			final boolean preservation = Utils.stringToBool(object.getProperty("preservation"), false);

			if (preservation)
				newObject(object);
			else {
				if (deleteGridReplicas(object))
					object.updateReplicas();
			}
		}
	}

	@Override
	public String toString() {
		return "AsyncReplication(" + flag + ")";
	}

	private static class ShutdownTask implements Runnable {
		@Override
		public void run() {
			// nothing
		}
	}

	void shutDown() {
		int lastCount = 0;
		int count;

		asyncReplicationQueue.offer(new ShutdownTask());

		while ((count = asyncReplicationQueue.size()) > 1) {
			if (count != lastCount) {
				System.err.println("Waiting for the last " + (count - 1) + " objects to be migrated");
				lastCount = count;
			}

			try {
				Thread.sleep(1000);
			}
			catch (@SuppressWarnings("unused") InterruptedException e) {
				// ignore
			}
		}
	}
}
