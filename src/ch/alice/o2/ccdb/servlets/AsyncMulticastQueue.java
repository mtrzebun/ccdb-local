/**
 * Cache warming tool. For existing objects it implements fetching the binary content from AliEn (in case the local file is missing) and
 * sending the content via multicast to the configured targets.
 *
 * Currently only the SQL-backed implementation can make use of it (as is the only one aware of AliEn)
 */
package ch.alice.o2.ccdb.servlets;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import alien.shell.commands.JAliEnCOMMander;
import lazyj.DBFunctions;
import lazyj.cache.ExpirationCache;
import lazyj.commands.SystemCommand;

/**
 * @author costing
 * @since Feb 5, 2021
 */
public class AsyncMulticastQueue {
	private static final Monitor monitor = MonitorFactory.getMonitor(AsyncMulticastQueue.class.getCanonicalName());

	private static Logger logger = Logger.getLogger(SQLBacked.class.getCanonicalName());

	private static ExpirationCache<UUID, UUID> recentlyBroadcastedObjects = new ExpirationCache<>();

	private static BlockingQueue<SQLObject> toMulticastQueue = new LinkedBlockingQueue<>(1000);

	private static BlockingQueue<SQLObject> toStageQueue = new LinkedBlockingQueue<>(1000);

	private static SQLtoUDP sender = SQLtoUDP.getInstance();

	/**
	 * Don't send the same object for 30s after a multicast operation
	 */
	private static final long COOLOFF_PERIOD = 1000L * 30;

	/**
	 * Blocking operation to stage (if needed) and unicast and/or multicast the content of object
	 *
	 * @param obj Object to send
	 * @return A strictly positive value if some action was taken.
	 *         <ul>
	 *         <li>-2 : error downloading the Grid replica</li>
	 *         <li>-1 : no UDP destination configured</li>
	 *         <li>0 : object was recently sent, not resending it until the cooloff period is reached (30s)</li>
	 *         <li>1 : object was present on the machine and was advertised on UDP</li>
	 *         <li>2 : the object had first to be downloaded from Grid and then it was sent on UDP</li>
	 *         </ul>
	 */
	public static int syncStageAndMulticast(final SQLObject obj) {
		if (!SQLBacked.udpSender() || sender == null)
			return -1;

		if (recentlyBroadcastedObjects.get(obj.id) != null)
			return 0;

		recentlyBroadcastedObjects.put(obj.id, obj.id, COOLOFF_PERIOD);

		int ret = 1;

		if (obj.getLocalFile(false) == null) {
			if (stage(obj)) {
				ret = 2;
			}
			else {
				logger.log(Level.WARNING, "Could not download the Grid replica of " + obj.getPath() + "/" + obj.validFrom + "/" + obj.id);
				return -2;
			}
		}

		sender.newObject(obj);

		return ret;
	}

	/**
	 * Queue an object to be staged from Grid (if not present locally) and sent by multicast. The operation is refused for repeated calls on the same object ID in less than 30s.
	 * Moreover the sending queue is limited in size to 1000 entries to limit the impact of accidental operations.
	 *
	 * @param obj
	 * @return <code>true</code> if the object was accepted for this operation, <code>false</code> if it was rejected
	 */
	public static boolean queueObject(final SQLObject obj) {
		if (recentlyBroadcastedObjects.get(obj.id) != null)
			return false;

		recentlyBroadcastedObjects.put(obj.id, obj.id, COOLOFF_PERIOD);

		if (obj.getLocalFile(false) != null) {
			// we have the local file, thus is can be sent by multicast
			if (!SQLBacked.udpSender())
				return false;

			return toMulticastQueue.offer(obj);
		}

		if (SQLBacked.gridBacking())
			return toStageQueue.offer(obj);

		return false;
	}

	private static Thread multicastSenderThread = new Thread("AsyncMulticastQueue.sender") {
		@Override
		public void run() {
			while (true) {
				SQLObject obj;
				try {
					obj = toMulticastQueue.take();
				}
				catch (@SuppressWarnings("unused") final InterruptedException e) {
					return;
				}

				if (obj != null && SQLBacked.udpSender() && sender != null) {
					logger.log(Level.INFO, "Broadcasting " + obj.getPath() + "/" + obj.id + " of " + obj.size + " bytes");

					sender.newObject(obj);
				}
			}
		}
	};

	private static Thread stagerThread = new Thread("AsyncMulticastQueue.stager") {
		@Override
		public void run() {
			while (true) {
				SQLObject obj;

				try {
					obj = toStageQueue.take();
				}
				catch (@SuppressWarnings("unused") final InterruptedException e) {
					return;
				}

				if (obj == null)
					continue;

				if (stage(obj))
					try {
						if (SQLBacked.udpSender())
							toMulticastQueue.put(obj);
					}
					catch (@SuppressWarnings("unused") final InterruptedException e) {
						return;
					}
			}
		}
	};

	private static JAliEnCOMMander commander = null;

	private static synchronized void initCommander() {
		if (commander == null)
			commander = new JAliEnCOMMander(null, null, "CERN", null);
	}

	/**
	 * @param obj
	 * @return <code>true</code> if the file is ready to be sent, <code>false</code> if any problem
	 */
	static boolean stage(final SQLObject obj) {
		if (obj == null)
			return false;

		if (obj.getLocalFile(false) != null)
			return true;

		File target = obj.getLocalFile(true);

		if (target == null)
			return false;

		String targetObjectPath = obj.getAddress(Integer.valueOf(-1), null, false).iterator().next();

		if (targetObjectPath == null || !targetObjectPath.startsWith("alien://"))
			return false;

		targetObjectPath = targetObjectPath.substring(8);

		initCommander();

		logger.log(Level.INFO, "Downloading missing file " + targetObjectPath + " to " + target.getAbsolutePath());

		try (Timing t = new Timing(monitor, "stage_in_ms")) {
			try {
				commander.c_api.downloadFile(targetObjectPath, target);
			}
			catch (@SuppressWarnings("unused") final IOException ioe) {
				return false;
			}
		}

		target = obj.getLocalFile(false);

		if (target != null) {
			// checks have passed, we have the file
			obj.replicas.add(Integer.valueOf(0));

			try (DBFunctions db = SQLObject.getDB()) {
				db.query("update ccdb set replicas=replicas || ? where id=? AND NOT ? = ANY(replicas);", false, Integer.valueOf(0), obj.id, Integer.valueOf(0));
			}

			// TODO replace with a native call if possible
			SystemCommand.executeCommand(List.of("chmod", "+t", target.getAbsolutePath()), false);

			return true;
		}

		return false;
	}

	static {
		if (SQLBacked.udpSender() && sender != null) {
			multicastSenderThread.setDaemon(true);
			multicastSenderThread.start();

			stagerThread.setDaemon(true);
			stagerThread.start();
		}
	}
}
