package ch.alice.o2.ccdb;

/**
 * @author costing
 * @since 2023-06-20
 */
public class InverseRegexOperator extends ConstraintOperator {
	/**
	 * @param value
	 */
	public InverseRegexOperator(String value) {
		super(value);
	}

	@Override
	public String toSQLExpression() {
		return "~";
	}

	@Override
	public boolean matches(String s) {
		return value.matches(s);
	}

	@Override
	public String getURLExpression() {
		return "=";
	}

	@Override
	public boolean isMeaningful() {
		if (value == null || value.isBlank())
			return false;

		return true;
	}

	@Override
	public String toSQLValue() {
		return value;
	}
}
