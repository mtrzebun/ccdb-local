package ch.alice.o2.ccdb;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * @author costing
 * @since 2023-06-20
 */
public class RegexOperator extends ConstraintOperator {
	private Pattern p = null;

	/**
	 * @param value
	 */
	public RegexOperator(String value) {
		super(value);
	}

	@Override
	public String toSQLExpression() {
		return "~";
	}

	@Override
	public boolean matches(String s) {
		if (p == null)
			p = Pattern.compile(value);

		return p.matcher(s).matches();
	}

	@Override
	public String getURLExpression() {
		return "~=";
	}

	@Override
	public boolean isMeaningful() {
		if (value == null || value.isBlank())
			return false;

		if (p == null) {
			try {
				p = Pattern.compile(value);
			}
			catch (@SuppressWarnings("unused") final PatternSyntaxException pse) {
				// ignore
				return false;
			}
		}

		return p != null;
	}

	@Override
	public String toSQLValue() {
		return "^" + value + "$";
	}
}
