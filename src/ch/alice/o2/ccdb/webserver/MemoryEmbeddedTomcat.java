package ch.alice.o2.ccdb.webserver;

import javax.servlet.ServletException;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.Wrapper;

import alien.config.ConfigUtils;
import ch.alice.o2.ccdb.Options;
import ch.alice.o2.ccdb.multicast.UDPReceiver;
import ch.alice.o2.ccdb.servlets.JSRoot;
import ch.alice.o2.ccdb.servlets.Memory;
import ch.alice.o2.ccdb.servlets.MemoryBrowse;
import ch.alice.o2.ccdb.servlets.MemoryDownload;
import ch.alice.o2.ccdb.servlets.MemoryProxy;
import ch.alice.o2.ccdb.servlets.MonitorServlet;

/**
 * Start an embedded Tomcat serving in-memory content only
 *
 * @author costing
 * @since 2017-09-26
 */
public class MemoryEmbeddedTomcat {

	/**
	 * @param args
	 * @throws ServletException
	 */
	public static void main(final String[] args) throws ServletException {
		ConfigUtils.setApplicationName("ccdb-memory");

		EmbeddedTomcat tomcat;

		try {
			tomcat = new EmbeddedTomcat("localhost");
		}
		catch (final ServletException se) {
			System.err.println("Cannot create the Tomcat server: " + se.getMessage());
			return;
		}

		final Wrapper browser = tomcat.addServlet(MemoryBrowse.class.getName(), "/browse/*");
		browser.addMapping("/latest/*");

		if (Options.getIntOption("MemoryProxy", 0) > 0)
			tomcat.addServlet(MemoryProxy.class.getName(), "/*");
		else
			tomcat.addServlet(Memory.class.getName(), "/*");

		tomcat.addServlet(MemoryDownload.class.getName(), "/download/*");

		tomcat.addServlet(MonitorServlet.class.getName(), "/monitor/*");
		tomcat.addServlet(JSRoot.class.getName(), "/JSRoot");

		// Start the server
		try {
			tomcat.start();
		}
		catch (final LifecycleException le) {
			System.err.println("Cannot start the Tomcat server: " + le.getMessage());
			return;
		}

		if (tomcat.debugLevel >= 1)
			System.err.println("Ready to accept HTTP calls on " + tomcat.addressList + ":" + tomcat.getPort());

		new UDPReceiver().start();

		tomcat.blockWaiting(); // inainte de asta run multicastReceiver
	}
}
